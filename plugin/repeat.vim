function! DuplicateWordAbove()
    let lineNr = line('.')
    if lineNr > 1
        let line =  getline(lineNr - 1)
        let startChar = charcol('.')
        let lineLen = strcharlen(line)
        if lineLen >= startChar
"            let char = nr2char(strgetchar(line, startChar - 1))
            let endChar = matchend(line, '\w*', startChar)
            if startChar == 1
                let startChar = 0
            endif
            let newStr = strcharpart(line, startChar, endChar - startChar)
            execute "normal! a" .. newStr .. "\<C-[>"
        endif
    endif
endfunction

" inoremap <C-T> <esc>:call DuplicateWordAbove()<cr>a
